import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


public class TestDemo {

    @Test
    public void sampleTest() {
        assertThat(1 + 1, is(equalTo(2)));
    }
}
